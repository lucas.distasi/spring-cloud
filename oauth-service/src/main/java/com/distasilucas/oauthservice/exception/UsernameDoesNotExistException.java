package com.distasilucas.oauthservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UsernameDoesNotExistException extends UsernameNotFoundException {

    public UsernameDoesNotExistException(String message) {
        super(message);
    }
}

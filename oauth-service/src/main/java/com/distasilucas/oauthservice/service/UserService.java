package com.distasilucas.oauthservice.service;


import brave.Tracer;
import com.distasilucas.oauthservice.client.UserClient;
import com.distasilucas.oauthservice.exception.UsernameDoesNotExistException;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService implements UserDetailsService, IUserService {

    private final UserClient userClient;
    private final Tracer tracer;

    @Value("${config.security.oauth.client.id}")
    private String clientId;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameDoesNotExistException {
        if (username.equalsIgnoreCase(clientId)) {
            log.info("Login error. User tried to login with clientId [{}]", clientId);
            throw new UsernameDoesNotExistException("User does not exists");
        }

        com.distasilucas.commons.entity.User user;
        List<GrantedAuthority> authorities = new ArrayList<>();

        try {
            user = userClient.findByUsernameOrEmail(username.toLowerCase());
            user.getRoles().forEach(rol -> {
                authorities.add(new SimpleGrantedAuthority(rol.getRole()));
            });

            return new User(user.getUsername(), user.getPassword(), authorities);
        } catch (FeignException ex) {
            String message = String.format("Username [%s] does not exists", username);
            log.info(message);
            tracer.currentSpan().tag("error.message", message);

            throw new UsernameDoesNotExistException("Username does not exists");
        } catch (Exception ex) {
            String message = String.format("Unhandled exception: %s", ex.getMessage());
            log.error(message);
            tracer.currentSpan().tag("error.message", message);

            throw new UsernameDoesNotExistException(message);
        }
    }

    @Override
    public com.distasilucas.commons.entity.User findByUsernameOrEmail(String param) {
        return userClient.findByUsernameOrEmail(param);
    }
}

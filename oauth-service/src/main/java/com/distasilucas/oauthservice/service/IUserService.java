package com.distasilucas.oauthservice.service;

import com.distasilucas.commons.entity.User;

public interface IUserService {

    User findByUsernameOrEmail(String param);
}

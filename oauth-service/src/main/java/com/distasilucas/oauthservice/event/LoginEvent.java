package com.distasilucas.oauthservice.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class LoginEvent implements AuthenticationEventPublisher {

    @Value("${config.security.oauth.client.id}")
    private String clientId;

    @Override
    public void publishAuthenticationSuccess(Authentication authentication) {
        UserDetails user = (UserDetails) authentication.getPrincipal();

        if (user.getUsername().equalsIgnoreCase(clientId))
            return;

        log.info("User [{}] successfully logged in", user.getUsername());
    }

    @Override
    public void publishAuthenticationFailure(AuthenticationException exception, Authentication authentication) {
        log.info("User [{}] does not exists", authentication.getPrincipal());
    }
}

package com.distasilucas.oauthservice.config;

import com.distasilucas.commons.entity.User;
import com.distasilucas.oauthservice.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.HashMap;
import java.util.Map;

@Configuration
@AllArgsConstructor
@Slf4j
public class TokenInformationConfig implements TokenEnhancer {

    private final UserService userService;

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        Map<String, Object> map = new HashMap<>();

        User user = userService.findByUsernameOrEmail(authentication.getName());
        map.put("email", user.getEmail());
        map.put("isBanned", String.valueOf(user.getBanned()));

        ((DefaultOAuth2AccessToken)accessToken).setAdditionalInformation(map);

        return accessToken;
    }
}

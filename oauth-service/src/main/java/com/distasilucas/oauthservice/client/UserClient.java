package com.distasilucas.oauthservice.client;

import com.distasilucas.commons.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "user-service")
public interface UserClient {

    @GetMapping("/users/{user}")
    User findByUsernameOrEmail(@PathVariable(name = "user") String user);
}

package com.distasilucas.productservice.repository;

import com.distasilucas.commons.entity.Product;
import org.springframework.data.repository.CrudRepository;

public interface IProductRepository extends CrudRepository<Product, String> {
}

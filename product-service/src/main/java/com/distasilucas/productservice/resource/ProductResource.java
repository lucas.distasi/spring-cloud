package com.distasilucas.productservice.resource;

import com.distasilucas.commons.entity.Product;
import com.distasilucas.productservice.exception.ProductNotFoundException;
import com.distasilucas.productservice.service.IProductService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/products")
@AllArgsConstructor
public class ProductResource {

    private final IProductService productService;

    @GetMapping
    public List<Product> getAllProducts() {
        return productService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> findProductById(@PathVariable String id) {
        Product product;

        try {
            product = productService.findById(id);
        } catch (ProductNotFoundException ex) {
            ex.getStackTrace();
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(product);
    }

    @PostMapping
    public ResponseEntity<Product> addProduct(@RequestBody Product product) {
        Product prod = new Product(product.getName(), product.getPrice());
        return ResponseEntity.status(HttpStatus.CREATED).body(productService.add(prod));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Product> deleteById(@PathVariable String id) {
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(productService.deleteById(id));
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Product> modifyProduct(@PathVariable String id,
                                                 @RequestBody Product product) {
        Product newProduct = new Product(id, product.getName(), product.getPrice());

        return ResponseEntity.status(HttpStatus.CREATED).body(productService.update(newProduct));
    }

}

package com.distasilucas.productservice.service;

import com.distasilucas.commons.entity.Product;
import com.distasilucas.productservice.exception.ProductNotFoundException;
import com.distasilucas.productservice.repository.IProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@AllArgsConstructor
public class ProductService implements IProductService {

    private final IProductRepository productRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Product> findAll() {
        return (List<Product>) productRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Product findById(String id) {
        return productRepository.findById(id).orElseThrow(() ->
                new ProductNotFoundException(String.format("Product with id %s has not be found", id)));
    }

    @Override
    public Product add(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product deleteById(String id) {
        Product product = productRepository.findById(id).orElseThrow(() ->
                new ProductNotFoundException(String.format("Product with id %s has not be found", id)));

        productRepository.deleteById(id);
        return product;
    }

    @Override
    public Product update(Product product) {
        productRepository.findById(product.getId()).orElseThrow(() ->
                new ProductNotFoundException(String.format("Product with id %s has not be found", product.getId())));
        Product newProduct = new Product(product.getId(), product.getName(), product.getPrice());

        productRepository.save(newProduct);
        return newProduct;
    }
}

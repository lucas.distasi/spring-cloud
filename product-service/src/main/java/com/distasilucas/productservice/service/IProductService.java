package com.distasilucas.productservice.service;

import com.distasilucas.commons.entity.Product;

import java.util.List;

public interface IProductService {

    List<Product> findAll();
    Product findById(String id);
    Product add(Product product);
    Product deleteById(String id);
    Product update(Product product);
}

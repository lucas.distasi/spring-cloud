#!/bin/bash

# Author: Lucas Di Stasi

configserver=$(pwd)/config-server
eurekaserver=$(pwd)/eureka-server
zuulservice=$(pwd)/zuul-service
userservice=$(pwd)/user-service
oauthservice=$(pwd)/oauth-service
itemservice=$(pwd)/item-service
productservice=$(pwd)/product-service
zipkin=$(find ~ -name zipkin-server-\*.jar)

services=($configserver $eurekaserver $zuulservice $oauthservice $userservice $itemservice $productservice)

export RABBIT_ADDRESSES=localhost:5672
echo
echo "# ========================================================================== #"
echo "Changing directory to $zipkin"
echo "Starting Zipkin and then sleeping 5 seconds..."
echo "# ========================================================================== #"
echo
java -jar $zipkin &
sleep 5s;
			
for item in "${services[@]}";
do
    echo
    echo "# ========================================================================== #"
    echo "Changing directory to $item"
    cd $item
    echo "Starting service and then sleeping 15 seconds..."
    echo "# ========================================================================== #"
    echo
    ./gradlew bootRun &     
    sleep 15s; 
done


package com.distasilucas.itemservice.client;

import com.distasilucas.itemservice.model.Product;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "product-service", path = "products")
@RibbonClient(name = "product-service")
public interface ProductClient {

    @GetMapping
    List<Product> getAllProducts();

    @GetMapping("/{id}")
    ResponseEntity<Product> findProductById(@PathVariable String id);

}

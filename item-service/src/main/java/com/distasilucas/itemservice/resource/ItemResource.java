package com.distasilucas.itemservice.resource;

import com.distasilucas.itemservice.model.Item;
import com.distasilucas.itemservice.service.IItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/items")
@RequiredArgsConstructor
@RefreshScope
public class ItemResource {

    private final IItemService itemService;

    @Value("${service.env}")
    private String environment;

    private final Environment env;

    @GetMapping
    public ResponseEntity<List<Item>> getAllItems() {
        return ResponseEntity.ok(itemService.getAllItems());
    }

    @GetMapping("/{id}/quantity/{quantity}")
    public ResponseEntity<Item> getItem(@PathVariable String id,
                                        @PathVariable Integer quantity) {
        Item item;

        try {
            item = itemService.findProductById(id, quantity);
        } catch (HttpClientErrorException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        return ResponseEntity.ok(item);
    }

    @GetMapping("/environment")
    public String getConfig() {

        return String.format("env.getProperty(\"service.env\"); -> %s " +
                "@Value(\"${service.env}\") -> %s " +
                "env.getActiveProfiles() -> %s", env.getProperty("service.env"), environment, Arrays.toString(env.getActiveProfiles()));
    }
}

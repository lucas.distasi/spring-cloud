package com.distasilucas.itemservice.service;

import com.distasilucas.itemservice.model.Item;
import com.distasilucas.itemservice.model.Product;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service("restTemplate")
@AllArgsConstructor
public class ItemService implements IItemService {

    private static final String PATH = "http://product-service/api-product/products";

    private final RestTemplate restTemplate;

    @Override
    public List<Item> getAllItems() {
        List<Product> products = Arrays.asList(restTemplate.getForObject(PATH, Product[].class));

        return products.stream().map(product ->
            new Item(product, 1)
        ).collect(Collectors.toList());
    }

    @Override
    public Item findProductById(String id, Integer quantity) {
        Map<String, String> pathVariable = new HashMap<>();
        pathVariable.put("id", id);
        Product product = restTemplate.getForObject(PATH + "/{id}", Product.class, pathVariable);

        return new Item(product, quantity);
    }
}

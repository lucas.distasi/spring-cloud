package com.distasilucas.itemservice.service;

import com.distasilucas.itemservice.model.Item;

import java.util.List;

public interface IItemService {

    List<Item> getAllItems();
    Item findProductById(String id, Integer quantity);
}

package com.distasilucas.itemservice.service;

import com.distasilucas.itemservice.client.ProductClient;
import com.distasilucas.itemservice.model.Item;
import com.distasilucas.itemservice.model.Product;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Primary
@Slf4j
@AllArgsConstructor
public class ItemClientService implements IItemService {

    private final ProductClient client;

    @Override
    public List<Item> getAllItems() {
        log.info("================================================================================================");
        log.info("////////////////////////////////// GOING THROUGH FEIGN CLIENT //////////////////////////////////");
        log.info("================================================================================================");
        return client.getAllProducts().stream().map(product ->
                new Item(product, 1)).collect(Collectors.toList());
    }

    @Override
    @HystrixCommand(fallbackMethod = "defaultItem")
    public Item findProductById(String id, Integer quantity) {
        return new Item(client.findProductById(id).getBody(), quantity);
    }

    private Item defaultItem(String id, Integer quantity) {
        return new Item(Product.defaultProduct(), 99);
    }
}

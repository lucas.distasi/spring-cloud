package com.distasilucas.itemservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@Getter
public class Product {

    private String id;
    private String name;
    private Double price;
    private Date createdAt;

    public static Product defaultProduct() {
        return new Product();
    }

    private Product() {
        this.id = "0";
        this.name = "NULL";
        this.price = 999d;
        this.createdAt = new Date();
    }
}
package com.distasilucas.itemservice.model;

import lombok.Getter;

@Getter
public class Item {

    private Product product;
    private Integer quantity;
    private Double price;

    private Item() {

    }

    public Item(Product product, Integer quantity) {
        this.product = product;
        this.quantity = quantity;
        this.price = quantity * product.getPrice();
    }
}
package com.distasilucas.userservice.resource;

import com.distasilucas.commons.entity.User;
import com.distasilucas.userservice.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@Slf4j
public class UserResource {

    private final UserRepository userRepository;

    @PostMapping("/users")
    public ResponseEntity<User> save(@RequestBody User u) {
        User user = new User(u.getUsername(), u.getEmail(), u.getPassword());
        return ResponseEntity.status(HttpStatus.CREATED).body(userRepository.save(user));
    }
}

package com.distasilucas.userservice.repository;

import com.distasilucas.commons.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@RepositoryRestResource(path = "users")
public interface UserRepository extends PagingAndSortingRepository<User, String> {

    @Query("SELECT u FROM User u WHERE u.username = ?1 OR u.email = ?1")
    @RestResource(path = "user")
    User findByUsernameOrEmail(@Param("param") String param);
}

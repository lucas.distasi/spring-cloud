INSERT INTO USERS (username, email, password, banned) VALUES ('lucas', 'lucas@hotmail.com', '$2y$17$escjSvu0llLBfJx/KsFn.ew0uY57LIASQJ231cxndQsCVn42W6OKG', false)
INSERT INTO USERS (username, email, password, banned) VALUES ('admin', 'admin@hotmail.com', '$2y$10$o1TZcsUXXGwjNH6dju./su0SYed.yuN0R4yDnCMwsYZ0ZZWmfryXi', false)

INSERT INTO ROLES (id_role, role_name) VALUES (1, 'ROLE_USER')
INSERT INTO ROLES (id_role, role_name) VALUES (2, 'ROLE_MODERATOR')
INSERT INTO ROLES (id_role, role_name) VALUES (3, 'ROLE_ADMIN')
INSERT INTO ROLES (id_role, role_name) VALUES (4, 'ROLE_ROOT')

INSERT INTO USER_ROLE (user_id, role_id) VALUES ('lucas', 1)
INSERT INTO USER_ROLE (user_id, role_id) VALUES ('lucas', 2)
INSERT INTO USER_ROLE (user_id, role_id) VALUES ('lucas', 3)
INSERT INTO USER_ROLE (user_id, role_id) VALUES ('lucas', 4)

INSERT INTO USER_ROLE (user_id, role_id) VALUES ('admin', 1)
INSERT INTO USER_ROLE (user_id, role_id) VALUES ('admin', 2)
INSERT INTO USER_ROLE (user_id, role_id) VALUES ('admin', 3)
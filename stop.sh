#!/bin/bash

# Author: Lucas Di Stasi

oauthservice=$(pgrep -f $(pwd)/oauth-service/build)
productservice=$(pgrep -f $(pwd)/product-service/build)
itemservice=$(pgrep -f $(pwd)/item-service/build)
userservice=$(pgrep -f $(pwd)/user-service/build)
zuulservice=$(pgrep -f $(pwd)/zuul-service/build)
eurekaserver=$(pgrep -f $(pwd)/eureka-server/build)
configserver=$(pgrep -f $(pwd)/config-server/build)
zipkin=$(pgrep -f zipkin)


declare -A services=(["zipkin"]=$zipkin
			["oauth-service"]=$oauthservice
			["product-service"]=$productservice
			["item-service"]=$itemservice
			["user-service"]=$userservice
			["zuul-service"]=$zuulservice
			["eureka-server"]=$eurekaserver
			["config-server"]=$configserver)

for item in "${!services[@]}";
do
    if [ -n "${services[$item]}" ]
    then
        echo "# ========================================================================== #"
        echo "Stopping service $item... | PID: ${services[$item]}"
        echo "# ========================================================================== #"
        kill ${services[$item]}
    fi
done


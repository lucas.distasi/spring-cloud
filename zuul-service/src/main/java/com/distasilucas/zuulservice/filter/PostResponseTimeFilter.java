package com.distasilucas.zuulservice.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
@Slf4j
public class PostResponseTimeFilter extends ZuulFilter {
    @Override
    public String filterType() {
        return "post";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        HttpServletRequest request = RequestContext.getCurrentContext().getRequest();
        Long requestStartTime = (Long)request.getAttribute("startTime");
        long requestFinishTime = System.currentTimeMillis();
        double totalTime = ((double) requestFinishTime - requestStartTime.doubleValue()) / 1000;

        log.info(String.format("<<<<<<<<<<<<<<<<<< TOTAL EXECUTION TIME %s SECONDS >>>>>>>>>>>>>>>>>>", totalTime));

        return null;
    }
}

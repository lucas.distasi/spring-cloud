package com.distasilucas.zuulservice.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
@Slf4j
public class PreResponseTimeFilter extends ZuulFilter {

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        HttpServletRequest request = RequestContext.getCurrentContext().getRequest();

        log.info(String.format("<<<<<<<<<<<<<<<<<< REQUEST ROUTED TO %s >>>>>>>>>>>>>>>>>>",
                request.getRequestURL().toString()));

        Long requestStartTime = System.currentTimeMillis();
        request.setAttribute("startTime", requestStartTime);
        return null;
    }
}

# Spring Cloud Project

> This is a basic project example used the techonologies listed below. Even if some services have a Dockerfile the project is not  made to run with Docker. Only Zuul Gateway URI's are protected with Spring Security. Remember this is just a sample project and is not even close to a production application. 

### Tech
* Java 11
* Gradle
* Spring Boot
* Spring Cloud
* Spring Data Rest
* Spring Security
* Zuul
* Netflix Eureka
* Zipkin

### Installation

This app requires Java 11, MariaDB, PostgreSQL, RabbitMQ installed in your local machine.
You can find in the repository two bash scripts (they are not going to work on Windows) to start or stop all the services needed.

After you clone the project you need to replace the username and password properties in the following files
* user-service-dev.yml (PostgreSQL)
* product-service-dev.yml (MariaDB)

You'll need [Zipkin](https://zipkin.io/pages/quickstart.html), just download the jar and put it somewhere in your computer, the start script should be able to find the file

Then with the following command you can start all the services without going through each one
```sh
$ ./start.sh
```

If you want to stop all the services just type
```sh
$ ./stop.sh
```

Below you can find the endpoints, request, response, authorization needed in order to retrieve information from the services.
Please, keep in mind that the endpoints listed are from the Gateway and not from each services (whose URI's are not protected)

&nbsp;
***
&nbsp;

| URI | Method | Authorization |
| ------ | ------ | ------ |
| localhost:8090/api-item/items | GET | NO

### Response

```json
[
  {
    "product": {
      "id": "ff80818175ed02030175ed79e06c0000",
      "name": "Coca Cola 2L",
      "price": 19.99,
      "createdAt": "2020-11-21T00:00:00.000+00:00"
    },
    "quantity": 1,
    "price": 19.99
  }
]
```

&nbsp;
***
&nbsp;


| URI | Method | Authorization |
| ------ | ------ | ------ |
| localhost:8090/api-item/items/ff80818175ed02030175ed79e06c0000/quantity/5 | GET | NO

### Response

```json
{
  "product": {
    "id": "ff80818175ed02030175ed79e06c0000",
    "name": "Coca Cola 2L",
    "price": 179.99,
    "createdAt": "2020-11-21T00:00:00.000+00:00"
  },
  "quantity": 5,
  "price": 899.95
}
```

If the ID does not exists, a custom object is returned
```json
{
  "product": {
    "id": "0",
    "name": "NULL",
    "price": 999,
    "createdAt": "2020-12-03T23:01:24.353+00:00"
  },
  "quantity": 99,
  "price": 98901
}
```

&nbsp;
***
&nbsp;

| URI | Method | Authorization |
| ------ | ------ | ------ |
| localhost:8090/api-product/products | GET | NO

### Response

```json
[
  {
    "id": "ff80818175ed02030175ed79e06c0000",
    "name": "Coca Cola 2L",
    "price": 19.99,
    "createdAt": "2020-11-21"
  }
]
```

&nbsp;
***
&nbsp;

| URI | Method | Authorization |
| ------ | ------ | ------ |
| localhost:8090/api-product/products/ff80818175ed02030175ed79e06c0000 | GET | NO

### Response

```json
{
  "id": "ff80818175ed02030175ed79e06c0000",
  "name": "Coca Cola 2L",
  "price": 19.99,
  "createdAt": "2020-11-21"
}
```

If the product does not exists, a 404 is returned.

&nbsp;
***
&nbsp;

| URI | Method | Authorization |
| ------ | ------ | ------ |
| localhost:8090/api-oauth/oauth/token | POST | NO

#### To get the token you will need to set a Basic Auth Authorization in Postman with the following parameters

| Username | Password |
| ------ | ------ |
| angular-app | 9c4d51e3-040e-404d-9496-232fd5b46378 |

#### Then you need a body of type x-www-form-urlencoded with the followint key-value

| username | password | grant_type |
| ------ | ------ | ------ |
| lucas | lucas | password |

#### With the token you will be able to access some restricted services

&nbsp;
***
&nbsp;

| URI | Method | Authorization |
| ------ | ------ | ------ |
| localhost:8090/api-product/products | POST | YES

### Request

```json
{
  "name": "Test",
  "price": 99.99
}
```

### Response
```json
{
  "id": "ff808181762ac53501762ae9a1ab0000",
  "name": "Test",
  "price": 99.99,
  "createdAt": "2020-12-03T23:22:03.045+00:00"
}
```

&nbsp;
***
&nbsp;

| URI | Method | Authorization |
| ------ | ------ | ------ |
| localhost:8090/api-product/products/delete/ff80818175bea74a0175beaab0990008 | DELETE | YES

### Request

```json
{
  "id": "ff808181762ac53501762ae9a1ab0000",
  "name": "Test",
  "price": 99.99,
  "createdAt": "2020-12-03"
}
```

&nbsp;
***
&nbsp;

| URI | Method | Authorization |
| ------ | ------ | ------ |
| localhost:8090/api-product/products/update/f4a0c60b-6272-4c6c-8657-d97855c70118 | PUT | YES

### Request

```json
{
  "name": "New Name",
  "price": 14.15
}
```

### Response 
```json
{
  "id": "ff808181762ac53501762aed56830001",
  "name": "New Name",
  "price": 14.15,
  "createdAt": "2020-12-03T23:26:39.448+00:00"
}
```

&nbsp;
***
&nbsp;

| URI | Method | Authorization |
| ------ | ------ | ------ |
| localhost:8090/api-user/users/lucas | GET | NO

### Response 
```json
{
  "username": "lucas",
  "email": "lucas@hotmail.com",
  "roles": [
    {
      "id": 1,
      "role": "ROLE_USER"
    },
    {
      "id": 2,
      "role": "ROLE_MODERATOR"
    },
    {
      "id": 3,
      "role": "ROLE_ADMIN"
    },
    {
      "id": 4,
      "role": "ROLE_ROOT"
    }
  ],
  "password": "$2y$17$escjSvu0llLBfJx/KsFn.ew0uY57LIASQJ231cxndQsCVn42W6OKG",
  "banned": false,
  "_links": {
    "self": {
      "href": "http://192.168.0.34:33389/users/lucas"
    },
    "user": {
      "href": "http://192.168.0.34:33389/users/lucas"
    }
  }
}
```

&nbsp;
***
&nbsp;

| URI | Method | Authorization |
| ------ | ------ | ------ |
| localhost:8090/api-user/users | POST | NO

### Request 
```json
{
  "username": "Joe",
  "email": "Joe@hotmail.com",
  "roles": [
    {
      "id": 1,
      "role": "ROLE_USER"
    },
    {
      "id": 2,
      "role": "ROLE_MODERATOR"
    }
  ],
  "password": "joepassword",
  "banned": false
}
```

### Response
Notice that only USER role is added
```json
{
  "username": "joe",
  "email": "joe@hotmail.com",
  "roles": [
    {
      "id": 1,
      "role": "ROLE_USER"
    }
  ],
  "password": "joepassword",
  "banned": false
}
```

&nbsp;
***
&nbsp;

| URI | Method | Authorization |
| ------ | ------ | ------ |
| localhost:8090/api-user/users/search/user?param=lucas@hotmail.com | GET | NO

You can use either username or email

### Response 
```json
{
  "username": "lucas",
  "email": "lucas@hotmail.com",
  "roles": [
    {
      "id": 1,
      "role": "ROLE_USER"
    },
    {
      "id": 2,
      "role": "ROLE_MODERATOR"
    },
    {
      "id": 3,
      "role": "ROLE_ADMIN"
    },
    {
      "id": 4,
      "role": "ROLE_ROOT"
    }
  ],
  "password": "$2y$17$escjSvu0llLBfJx/KsFn.ew0uY57LIASQJ231cxndQsCVn42W6OKG",
  "banned": false,
  "_links": {
    "self": {
      "href": "http://192.168.0.34:33389/users/lucas"
    },
    "user": {
      "href": "http://192.168.0.34:33389/users/lucas"
    }
  }
}
```

## And that's all for now! I might add more things in the future
